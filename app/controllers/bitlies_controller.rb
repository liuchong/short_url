class BitliesController < ApplicationController
  def index
    hash = nil
    if params[:url]
      authorize = UrlShortener::Authorize.new 'liuchong14',
          'R_725a2f12e1dd7c6822c82510be035c8c'
      client = UrlShortener::Client.new(authorize)
      hash = shorten = client.shorten(params[:url]).hash
    end

    respond_to do |fmt|
      fmt.json { render :json => { hash: hash } }
    end
  end
end
